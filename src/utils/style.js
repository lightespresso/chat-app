export const ifProp = (prop, onTrue, onFalse = 'unset') => props =>
  props[prop] ? onTrue : onFalse

export const caseProp = (prop, cases, defaultValue = 'unset') => props => {
  const propValue = props[prop]
  return cases[propValue] || defaultValue
}
