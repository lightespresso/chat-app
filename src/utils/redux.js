import R from 'ramda'
import { bindActionCreators } from 'redux'
import { connect as ReactReduxConnect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

/**
 * Creates actionCreator of given type
 * @param {String} type
 * @param {Function} [fn = data => { payload: data }] - function, that transforms actionCreator's arguments to action body
 */
export const actionOf = (type, fn = payload => ({ payload })) => {
  if (!type) {
    throw new TypeError('Action type is undefined')
  }
  return function () {
    const body = fn(...arguments)
    return {
      type,
      ...body
    }
  }
}

/**
 * Creates a reducer
 * @param {Any} initialState
 * @param {Object.<String, Function>} handlers { [type]: (payload => state) => newState }
 */
export const createReducer = (initialState, handlers) => {
  const uncurriedHandlers = R.map(R.uncurryN(2), handlers)
  return function (state = initialState, action) {
    const handler = uncurriedHandlers[action.type] || R.always(state)
    return handler(action.payload, state)
  }
}

export const connect = (selectors, actionCreators, mergeProps) =>
  ReactReduxConnect(
    selectors && createStructuredSelector(selectors),
    actionCreators &&
      (dispatch => bindActionCreators(actionCreators, dispatch)),
    mergeProps
  )
