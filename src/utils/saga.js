import R from 'ramda'
import { all, fork } from 'redux-saga/effects'

/**
 * Creates rootSaga from sagas list or object
 * @param {Object|Array} sagas
 * @return {Saga}
 */
export const combineSagas = sagas => {
  const sagasList = R.values(sagas)
  return function * rootSaga () {
    yield all(sagasList.map(R.when(R.is(Function), fork)))
  }
}
