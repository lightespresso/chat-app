import R from 'ramda'

/**
 * Splits list when two adjacent items satisfies predicate
 * 
 * @param {Function} pred (a, b) -> bool
 * @param {Array} list 
 * @returns 
 */
export const splitBy = R.curry((pred, list) => {
  let lastItem
  return R.splitWhen(item => {
    if (!lastItem || !pred(item, lastItem)) {
      lastItem = item
      return false
    }
    return true
  }, list)
})

/**
 * Splits list in sequences, where all sublist's items satisfies predicate
 * e.g. stackBy(R.equals, [1, 2, 2, 3]) -> [[1], [2, 2], [3]]
 * @param {any} pred 
 * @param {any} list 
 * @returns 
 */
export const stackBy = R.curry((pred, list) => {
  const splitByPred = splitBy(R.complement(pred))
  const splitStacks = list => {
    const stacks = splitByPred(list)
    const lastStack = R.last(stacks)
    if (!lastStack.length) {
      return R.init(stacks)
    } else {
      return [...R.init(stacks), ...splitStacks(lastStack)]
    }
  }
  return list.length ? splitStacks(list) : []
})
