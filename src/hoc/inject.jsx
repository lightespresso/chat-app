import React from 'react'

const inject = injectProps => Component => {
  const WithInjectedProps = props => <Component {...injectProps} {...props} />
  return WithInjectedProps
}

export default inject
