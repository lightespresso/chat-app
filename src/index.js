import React from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import Root from './root.jsx'
import configureStore from './state/configure-store'

import 'roboto-fontface'
import 'rmc-date-picker/assets/index.css'
import 'rmc-picker/assets/index.css'

const store = configureStore()

ReactDOM.render(<Root store={store} />, document.getElementById('root'))

if (module.hot) {
  module.hot.accept('./root.jsx', () => {
    const NextRoot = require('./root.jsx')
    ReactDOM.render(<NextRoot store={store} />, document.getElementById('root'))
  })
}
