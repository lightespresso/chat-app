import { combineSagas } from 'utils/saga'
import { types, receiveMessage } from './actions'
import { take, put, call } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import shortid from 'shortid'

import fakeMessages from './fake-messages.json'

function * sendFakeMessages (messages) {
  let i = 0
  while (i < messages.length) {
    const message = messages[i]
    yield delay(1000)
    yield put(receiveMessage({ id: shortid(), ...message }))
    i += 1
  }
}

function * produceFakeData (messageIndex) {
  const messages = fakeMessages[messageIndex]
  yield call(sendFakeMessages, messages)
  yield take(types.MESSAGE_SENT)
  const nextMessageIndex =
    messageIndex + 1 < fakeMessages.length ? messageIndex + 1 : 0
  yield call(produceFakeData, nextMessageIndex)
}

export default combineSagas([call(produceFakeData, 0)])
