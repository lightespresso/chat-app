import R from 'ramda'
import { createReducer } from 'utils/redux'
import { types } from './actions'

export const userData = createReducer(
  {
    photo: 'http://fillmurray.com/300/300',
    name: 'John Doe'
  },
  {}
)

export const companionUserData = createReducer(
  {
    photo: 'http://fillmurray.com/200/200',
    name: 'Steve Sterling',
    rating: 4,
    role: 'support manager'
  },
  {}
)

export const messages = createReducer([], {
  [types.MESSAGE_RECEIVED]: R.append,
  [types.MESSAGE_SENT]: R.append
})

export const interactionStack = createReducer([], {
  [types.MESSAGE_RECEIVED]: ({ interaction, id }) =>
    interaction ? R.append({ id, ...interaction }) : R.always,
  [types.MESSAGE_SENT]: ({ interactionId }) =>
    interactionId ? R.reject(R.whereEq({ id: interactionId })) : R.always
})
