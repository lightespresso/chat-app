import { createTypes } from 'redux-action-types'
import { actionOf } from 'utils/redux'
import shortid from 'shortid'

export const types = createTypes('chat/', 'MESSAGE_RECEIVED', 'MESSAGE_SENT')

export const sendMessage = actionOf(
  types.MESSAGE_SENT,
  ({ text, type = 'text', ...data }) => ({
    payload: {
      text,
      own: true,
      userId: 1,
      id: shortid(),
      type,
      ...data
    }
  })
)
export const receiveMessage = actionOf(types.MESSAGE_RECEIVED)
