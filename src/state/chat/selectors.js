import R from 'ramda'
import { createSelector } from 'reselect'
import { stackBy } from 'utils/fp'

const eqByUserId = R.eqBy(R.prop('userId'))

export const getMessageGroups = createSelector(
  state => state.chat.messages,
  R.compose(
    R.map(
      R.applySpec({
        id: R.path([0, 'id']),
        own: R.path([0, 'own']),
        messages: R.identity
      })
    ),
    stackBy(eqByUserId)
  )
)

export const getUserData = state => state.chat.userData
export const getCompanionUserData = state => state.chat.companionUserData
export const getLastInteraction = createSelector(
  state => state.chat.interactionStack,
  R.last
)
