import { combineReducers } from 'redux'

import * as chat from './chat/reducers'

export default {
  chat: combineReducers(chat)
}
