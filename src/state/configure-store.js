import createSagaMiddleware from 'redux-saga'
import { compose, combineReducers, createStore, applyMiddleware } from 'redux'
import { combineSagas } from 'utils/saga'

import reducers from './reducers'
import sagas from './sagas'

const DEV = process.env.NODE_ENV !== 'production'

const composeEnhansers =
  (DEV && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(
    combineReducers(reducers),
    composeEnhansers(applyMiddleware(sagaMiddleware))
  )

  if (DEV && module.hot) {
    module.hot.accept('./reducers.js', () => {
      const reducers = require('./reducers.js')
      store.replaceReducer(combineReducers(reducers))
    })
  }

  const rootSaga = combineSagas(sagas)
  sagaMiddleware.run(rootSaga)

  return store
}

export default configureStore
