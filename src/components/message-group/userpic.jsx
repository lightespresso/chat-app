import React from 'react'
import Avatar from 'components/avatar'
import { AppearFromLeft, AppearFromRight } from 'components/animation'

const Userpic = ({ right, ...props }) => {
  const Animation = right ? AppearFromRight : AppearFromLeft
  return <Animation>{style => <Avatar {...props} style={style} />}</Animation>
}

export default Userpic
