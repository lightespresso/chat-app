import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ifProp } from 'utils/style'

import Userpic from './userpic'
import Message from 'components/message'

class MessageGroup extends Component {
  static propTypes = {
    messages: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string,
        text: PropTypes.string,
        id: PropTypes.string
      })
    ).isRequired,
    userpic: PropTypes.string,
    own: PropTypes.bool
  }
  renderMessage = ({ type, id, ...message }) => {
    const { own } = this.props
    return <Message key={id} type={type} own={own} {...message} />
  }
  render () {
    const { userpic, own, messages } = this.props

    return (
      <MessageContainer right={own}>
        <Userpic src={userpic} size="small" right={own} />
        <Inner>{messages.map(this.renderMessage)}</Inner>
      </MessageContainer>
    )
  }
}

export default MessageGroup

const MessageContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-end;
  flex-direction: ${ifProp('right', 'row-reverse', 'row')};
  justify-content: flex-start;
`

const Inner = styled.div`width: 80%;`
