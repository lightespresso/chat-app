import styled from 'styled-components'

import markerIcon from './marker.svg'

const Marker = styled.div`
  width: 15px;
  height: 20px;
  background-image: url(${markerIcon});
  background-position: center;
  background-size: contain;
  transform: translate(-50%, -50%);
`

export default Marker
