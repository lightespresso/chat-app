import R from 'ramda'
import React from 'react'
import PropTypes from 'prop-types'
import GoogleMap from 'google-map-react'

import Marker from './marker'

const GM_TOKEN = process.env.REACT_APP_GOOGLE_MAPS_TOKEN

const Location = ({ position }) => {
  return (
    <GoogleMap apiKey={GM_TOKEN} defaultCenter={position} defaultZoom={11}>
      <Marker {...position} />
    </GoogleMap>
  )
}

Location.propTypes = {
  position: PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number
  }).isRequired
}

export default Location
