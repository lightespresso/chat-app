import React from 'react'
import styled from 'styled-components'
import { AppBar, IconButton, Toolbar } from 'material-ui'
import Typography from 'material-ui/Typography'
import { Menu as MenuIcon, Refresh as RefreshIcon } from 'material-ui-icons'
import colors from 'style/colors'
import { rgba } from 'polished'

const HEADER_COLOR = rgba(colors.white, 0.9)

const Header = () => (
  <Container position="fixed">
    <Toolbar>
      <IconButton>
        <MenuIcon />
      </IconButton>
      <Heading>Chat app</Heading>
      <IconButton>
        <RefreshIcon />
      </IconButton>
    </Toolbar>
  </Container>
)

export default Header

const Heading = styled(Typography).attrs({
  type: 'headline'
})`
  flex: 1;
  text-align: center;
`
const Container = styled(AppBar).attrs({
  position: 'fixed'
})`
  background-color: ${rgba(colors.white, 0.9)} !important;
`
