import Animate from './animate.jsx'
import inject from 'hoc/inject'

export default Animate

const BOUNCE_EASING = 'cubic-bezier(0.480, 0.005, 0.215, 1.560)'

export const AppearFromBottom = inject({
  entering: { opacity: 0, transform: 'translateY(50%)' },
  entered: { opacity: 1, transform: 'translateY(0)', transition: 'all 0.3s' }
})(Animate)

export const AppearFromLeft = inject({
  entering: { opacity: 0, transform: 'translateX(-100%)' },
  entered: { opacity: 1, transform: 'translateX(0)', transition: 'all 0.3s' }
})(Animate)

export const AppearFromRight = inject({
  entering: { opacity: 0, transform: 'translateX(100%)' },
  entered: { opacity: 1, transform: 'translateX(0)', transition: 'all 0.3s' }
})(Animate)

export const ZoomIn = inject({
  entering: { opacity: 0, transform: 'scale(0, 0)' },
  entered: {
    opacity: 1,
    transform: 'scale(1, 1)',
    transition: `all 0.3s ${BOUNCE_EASING}`
  }
})(Animate)

export const ZoomHorizontal = inject({
  entering: { opacity: 0, transform: 'scale(0, 1)' },
  entered: {
    opacity: 1,
    transform: 'scale(1, 1)',
    transition: `all 0.3s ${BOUNCE_EASING}`
  }
})(Animate)
