import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Transition } from 'react-transition-group'

class Animate extends Component {
  static propTypes = {
    entering: PropTypes.object,
    entered: PropTypes.object,
    leaving: PropTypes.object,
    leaved: PropTypes.object,
    timeout: PropTypes.number,
    display: PropTypes.bool,
    displayOnMount: PropTypes.bool,
    children: PropTypes.func.isRequired
  }
  static defaultProps = {
    timeout: 0,
    displayOnMount: true,
    display: false
  }
  state = {
    display: this.props.display
  }
  getStyle = state => this.props[state] || {}
  componentDidMount () {
    const { displayOnMount } = this.props
    if (displayOnMount) {
      this.setState({ display: true })
    }
  }
  render () {
    const { children: renderChildren, timeout } = this.props
    const { display } = this.state
    return (
      <Transition in={display} timeout={timeout}>
        {state => {
          const style = this.getStyle(state)
          return renderChildren(style)
        }}
      </Transition>
    )
  }
}

export default Animate
