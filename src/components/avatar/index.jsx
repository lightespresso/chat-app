import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Avatar } from 'material-ui'
import colors from 'style/colors'
import { caseProp } from 'utils/style'

import placeholder from './placeholder.svg'

const UserPhoto = styled(Avatar)`
  width: ${caseProp('size', {
    small: '40px',
    medium: '70px',
    large: '100px'
  })} !important;
  height: ${caseProp('size', {
    small: '40px',
    medium: '70px',
    large: '100px'
  })} !important;
  margin-bottom: 0.3rem;
  background-color: ${colors.gray};
`

UserPhoto.propTypes = {
  src: PropTypes.string
}

UserPhoto.defaultProps = {
  src: placeholder,
  alt: 'User photo'
}

export default UserPhoto
