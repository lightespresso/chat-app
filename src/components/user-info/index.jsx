import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Typography from 'material-ui/Typography'
import Rating from 'react-star-rating-component'
import VisibilitySensor from 'react-visibility-sensor'

import Avatar from 'components/avatar'

import { AppearFromBottom } from 'components/animation'

const UserInfo = ({ onVisibleChange, name, photo, rating, role }) => {
  return (
    <VisibilitySensor onChange={onVisibleChange}>
      <AppearFromBottom>
        {style => (
          <Container style={style}>
            <Avatar size="large" src={photo} />
            <Typography type="caption">{name}</Typography>
            <Rating editing={false} name="stars" starCount={5} value={rating} />
            {role && <Typography type="caption">{role}</Typography>}
          </Container>
        )}
      </AppearFromBottom>
    </VisibilitySensor>
  )
}

UserInfo.propTypes = {
  name: PropTypes.string.isRequired,
  rating: PropTypes.number.isRequired,
  role: PropTypes.string,
  photo: PropTypes.string,
  onVisibleChange: PropTypes.func
}

export default UserInfo

const Container = styled.div`
  align-self: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 15px;
`
