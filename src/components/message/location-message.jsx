import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import MessageBubble from './message-bubble'
import Location from 'components/location'

const LocationMessage = ({ own, text, position, ...props }) => (
  <MessageBubble own={own} {...props}>
    <LocationContainer>
      <Location position={position} />
    </LocationContainer>
    {text}
  </MessageBubble>
)

LocationMessage.propTypes = {
  own: PropTypes.bool,
  text: PropTypes.string,
  position: PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number
  }).isRequired
}

export default LocationMessage

const LocationContainer = styled.div`
  height: 300px;
  margin: -10px -15px 10px -15px;
`
