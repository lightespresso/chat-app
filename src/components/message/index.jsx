import React from 'react'
import PropTypes from 'prop-types'

import { ZoomIn } from 'components/animation'

import TextMessage from './text-message'
import LocationMessage from './location-message'
import ProcessRequest from './process-request'

const messageTypes = {
  text: TextMessage,
  location: LocationMessage,
  processRequest: ProcessRequest
}

const Message = ({ type, ...props }) => {
  const Component = messageTypes[type]
  if (!Component) {
    throw new Error(`there is no component for message of type ${type}`)
  }
  return <ZoomIn>{style => <Component {...props} style={style} />}</ZoomIn>
}

Message.propTypes = {
  type: PropTypes.string.isRequired
}

export default Message
