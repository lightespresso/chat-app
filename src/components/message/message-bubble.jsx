import styled, { css } from 'styled-components'
import { ifProp } from 'utils/style'
import colors from 'style/colors'

const borders = {
  left: css`
    border-radius: 5px 20px 20px 5px;
    &:first-child {
      border-top-left-radius: 20px;
    }
    &:last-child {
      border-bottom-left-radius: 20px;
    }
  `,
  right: css`
    border-radius: 20px 5px 5px 20px;
    &:first-child {
      border-top-right-radius: 20px;
    }
    &:last-child {
      border-bottom-right-radius: 20px;
    }
  `
}

const MessageBubble = styled.div`
  overflow: hidden;
  margin: 5px;
  padding: 10px 15px;
  background-color: ${ifProp('own', colors.darkGray, colors.gray)};
  color: ${ifProp('own', colors.white, colors.black)};
  ${({ own }) => (own ? borders.right : borders.left)};
`

export default MessageBubble
