import R from 'ramda'
import React, { Component } from 'react'
import styled from 'styled-components'
import Rating from 'react-star-rating-component'
import { LinearProgress, Typography } from 'material-ui'
import { TransitionGroup } from 'react-transition-group'
import { ZoomHorizontal } from 'components/animation'

import MessageBubble from './message-bubble'

const requestSteps = [
  {
    text: 'Connecting to database',
    result: {
      label: 'Building Age',
      value: 3
    }
  },
  {
    text: 'Confirming structural materials',
    result: {
      label: 'Building durability',
      value: 5
    }
  },
  {
    text: 'Checking weather data',
    result: {
      label: 'Distance from coast',
      value: 3
    }
  },
  {
    text: 'Completed!',
    completed: true
  }
]

class ProcessRequest extends Component {
  state = {
    results: [],
    text: requestSteps[0].text
  }
  componentDidMount = () => {
    this.processStep(0)
  }
  processStep = index => {
    const step = requestSteps[index]
    const { text, result, completed } = step
    this.setState({
      text
    })
    if (!completed) {
      this.__timeout = window.setTimeout(() => {
        this.setState(
          R.evolve({
            results: R.append(result)
          })
        )
        this.processStep(index + 1)
      }, 2000)
    }
  }
  get progressValue () {
    return this.state.results.length / (requestSteps.length - 1) * 100
  }
  renderResult = ({ label, value }) => (
    <ZoomHorizontal key={label}>
      {style => (
        <Result style={style}>
          {label}
          <Rating name="start" editing={false} starCount={5} value={value} />
        </Result>
      )}
    </ZoomHorizontal>
  )
  render () {
    const { text, own, results } = this.state
    return (
      <MessageBubble>
        <Typography type="subheading">{text}</Typography>
        <LinearProgress mode="determinate" value={this.progressValue} />
        <TransitionGroup>{results.map(this.renderResult)}</TransitionGroup>
      </MessageBubble>
    )
  }
}

export default ProcessRequest

const Result = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 0;
`
