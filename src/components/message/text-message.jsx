import React from 'react'
import PropTypes from 'prop-types'

import MessageBubble from './message-bubble'

const TextMessage = ({ text, own, ...props }) => (
  <MessageBubble own={own} {...props}>
    {text}
  </MessageBubble>
)

TextMessage.propTypes = {
  text: PropTypes.string.isRequired,
  own: PropTypes.bool
}

export default TextMessage
