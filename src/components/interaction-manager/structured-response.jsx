import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Button } from 'material-ui'

class StructuredResponseInteraction extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string,
        data: PropTypes.any
      })
    ).isRequired,
    id: PropTypes.string
  }
  handleSubmit = ({ data, text }) => () => {
    const { id: interactionId, onSubmit } = this.props
    onSubmit({ interactionId, data, text })
  }
  renderOption = ({ text, data }) => {
    return (
      <Button key={data} onClick={this.handleSubmit({ text, data })}>
        {text}
      </Button>
    )
  }
  render () {
    const { options } = this.props
    return <Container>{options.map(this.renderOption)}</Container>
  }
}

export default StructuredResponseInteraction

const Container = styled.div`
  display: flex;
  justify-content: space-around;
`
