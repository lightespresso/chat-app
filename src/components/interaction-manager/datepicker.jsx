import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Datepicker from 'rmc-date-picker'
import { Button } from 'material-ui'
import moment from 'moment'

class DatepickerInteraction extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired
  }
  state = {
    date: new Date()
  }
  handleChange = date => this.setState({ date })
  handleSubmit = () => {
    const { onSubmit, id: interactionId } = this.props
    const { date } = this.state
    const text = moment(date).format('DD.MM.YYYY')
    onSubmit({
      interactionId,
      text,
      date: +date
    })
  }
  render () {
    return (
      <Container>
        <Datepicker onDateChange={this.handleChange} />
        <SubmitBtn onClick={this.handleSubmit}>Submit</SubmitBtn>
      </Container>
    )
  }
}

export default DatepickerInteraction

const Container = styled.div`
  height: 300px;
  display: flex;
  flex-direction: column;
`

const SubmitBtn = styled(Button)`align-self: center;`
