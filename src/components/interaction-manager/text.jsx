import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { TextField, IconButton } from 'material-ui'
import { Send } from 'material-ui-icons'

class TextInteraction extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    id: PropTypes.number
  }
  state = {
    input: ''
  }
  handleChange = ({ target }) => {
    this.setState({
      text: target.value
    })
  }
  handleKeyPress = ({ key }) => {
    if (key === 'Enter') this.handleSubmit()
  }
  handleSubmit = () => {
    const { text } = this.state
    if (!text) return
    const { onSubmit, id: interactionId } = this.props
    onSubmit({
      interactionId,
      text
    })
  }
  render () {
    const { text } = this.state
    return (
      <Container>
        <TextField
          value={text}
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
          fullWidth
        />
        <SendBtn onClick={this.handleSubmit}>
          <Send />
        </SendBtn>
      </Container>
    )
  }
}

export default TextInteraction

const Container = styled.div`
  display: flex;
  align-items: center;
`

const SendBtn = styled(IconButton)`height: 32px !important;`
