import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { TextField } from 'material-ui'

import Suggestions from './suggestions'

class LocationInteraction extends Component {
  static propTypes = {
    id: PropTypes.string,
    onSubmit: PropTypes.func.isRequired
  }
  state = {
    text: '',
    hasSuggestions: false
  }
  handleSubmit = () => {
    const { id: interactionId, onSubmit } = this.props
    onSubmit({
      interactionId,
      text: 'Edinbourg',
      type: 'location',
      position: {
        lat: 55.953251,
        lng: -3.188267
      }
    })
  }
  handleChange = ({ target }) => {
    this.setState({
      text: target.value,
      hasSuggestions: true
    })
  }
  render () {
    const { text, hasSuggestions } = this.state
    return (
      <Container>
        {hasSuggestions && <Suggestions onSelect={this.handleSubmit} />}
        <TextField
          key="input"
          fullWidth
          value={text}
          onChange={this.handleChange}
        />
      </Container>
    )
  }
}

export default LocationInteraction

const Container = styled.div`position: relative;`
