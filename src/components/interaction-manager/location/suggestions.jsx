import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { List, ListItem } from 'material-ui'

const Suggestions = ({ onSelect }) => (
  <Container>
    <List>
      <ListItem onClick={onSelect} button>
        44-46 Morningside Road, Edinburgh, Scotland
      </ListItem>
      <ListItem onClick={onSelect} button>
        44-46 Morningside Road, Edinburgh, Scotland
      </ListItem>
      <ListItem onClick={onSelect} button>
        44-46 Morningside Road, Edinburgh, Scotland
      </ListItem>
    </List>
  </Container>
)

Suggestions.propTypes = {
  onSelect: PropTypes.func.isRequired
}

export default Suggestions

const Container = styled.div`
  background-color: #fff;
  position: absolute;
  bottom: 100%;
  left: 0;
  right: 0;
`
