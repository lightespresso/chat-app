import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Paper } from 'material-ui'

import TextInteraction from './text'
import StructuredResponse from './structured-response'
import Location from './location'
import Checkboxes from './check'
import Datepicker from './datepicker'

import { connect } from 'utils/redux'
import { getLastInteraction } from 'state/chat/selectors'
import { sendMessage } from 'state/chat/actions'

const interactionTypes = {
  text: TextInteraction,
  structuredResponse: StructuredResponse,
  location: Location,
  checkboxes: Checkboxes,
  date: Datepicker
}

const defaultInteraction = {
  type: 'text'
}

const InteractionManager = ({
  interaction = defaultInteraction,
  sendMessage
}) => {
  const { type, ...data } = interaction
  const Interaction = interactionTypes[type]
  if (!Interaction) {
    throw new Error(`there is no component for interaction of type ${type}`)
  }
  return (
    <Container>
      <Interaction {...data} onSubmit={sendMessage} />
    </Container>
  )
}

InteractionManager.propTypes = {
  interaction: PropTypes.object,
  sendMessage: PropTypes.func.isRequired
}

export default connect(
  {
    interaction: getLastInteraction
  },
  {
    sendMessage
  }
)(InteractionManager)

const Container = styled(Paper)`padding: 10px;`
