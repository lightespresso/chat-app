import R from 'ramda'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { List, ListItem, ListItemText, Checkbox, Button } from 'material-ui'
import styled from 'styled-components'

const getSelectedIndex = R.compose(R.map(R.F), R.indexBy(R.prop('value')))

class Checkboxes extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    onSubmit: PropTypes.func.isRequired
  }
  state = getSelectedIndex(this.props.options)
  handleToggle = key => () =>
    this.setState(
      R.evolve({
        [key]: R.not
      })
    )
  get selectedValues () {
    return R.compose(R.keys, R.filter(Boolean))(this.state)
  }
  get selectedText () {
    const { options } = this.props
    return this.selectedValues
      .map(value => R.find(R.whereEq({ value }), options).text)
      .join(' and ')
  }

  handleSubmit = () => {
    if (!this.selectedValues) return
    const { id: interactionId, onSubmit } = this.props
    onSubmit({
      interactionId,
      text: this.selectedText,
      data: this.selectedValues
    })
  }
  renderItem = ({ text, value }) => {
    const isChecked = this.state[value]
    return (
      <ListItem button key={value} onClick={this.handleToggle(value)}>
        <Checkbox disableRipple checked={isChecked} />
        <ListItemText primary={text} />
      </ListItem>
    )
  }
  render () {
    const { options } = this.props
    return (
      <Container>
        <List>{options.map(this.renderItem)}</List>
        <SubmitBtn onClick={this.handleSubmit}>Submit</SubmitBtn>
      </Container>
    )
  }
}

export default Checkboxes

const Container = styled.div`
  display: flex;
  flex-direction: column;
`

const SubmitBtn = styled(Button)`align-self: center;`
