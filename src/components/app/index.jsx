import React from 'react'
import styled from 'styled-components'

import Header from 'components/header'
import Chat from 'components/chat'
import InteractionManager from 'components/interaction-manager'

const App = () => (
  <Container>
    <Header />
    <Inner>
      <Chat />
      <InteractionManager />
    </Inner>
  </Container>
)

const Container = styled.div`
  height: 100vh;
  position: relative;
`

const Inner = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`

export default App
