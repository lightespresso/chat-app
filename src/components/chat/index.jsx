import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Scrollbars from 'react-custom-scrollbars'

import UserInfo from 'components/user-info'
import MessageGroup from 'components/message-group'

import { connect } from 'utils/redux'
import {
  getMessageGroups,
  getUserData,
  getCompanionUserData
} from 'state/chat/selectors'

class Chat extends Component {
  static propTypes = {
    messageGroups: PropTypes.array.isRequired,
    userData: PropTypes.object.isRequired,
    companionUserData: PropTypes.object.isRequired
  }
  componentDidUpdate ({ messageGroups }) {
    if (messageGroups !== this.props.messageGroups) {
      window.setTimeout(() => this._scrollbars.scrollToBottom(), 100)
    }
  }
  setScrollRef = scrollbars => {
    this._scrollbars = scrollbars
  }
  renderMessage = ({ id, messages, own }) => {
    const { userData, companionUserData } = this.props
    const user = own ? userData : companionUserData
    return (
      <MessageGroup
        key={id}
        userpic={user.photo}
        own={own}
        messages={messages}
      />
    )
  }
  render () {
    const { messageGroups, companionUserData: user } = this.props
    return (
      <Container>
        <Scrollbars ref={this.setScrollRef}>
          <Inner>
            <UserInfo
              photo={user.photo}
              name={user.name}
              role={user.role}
              rating={user.rating}
            />
            {messageGroups.map(this.renderMessage)}
          </Inner>
        </Scrollbars>
      </Container>
    )
  }
}

export default connect({
  messageGroups: getMessageGroups,
  userData: getUserData,
  companionUserData: getCompanionUserData
})(Chat)

const Container = styled.div`flex: 1;`

const Inner = styled.div`
  box-sizing: border-box;
  min-height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding: 100px 10px 0 10px;
`
