import { pink } from 'material-ui/colors'

export default {
  white: '#fff',
  black: '#222',
  gray: '#eee',
  darkGray: '#666',
  primary: pink
}
